from django.shortcuts import render
from django.contrib.auth.decorators import login_required
import qrcode
import qrcode.image.svg
import pyotp
from io import BytesIO


@login_required
def portal(request):

    totp = pyotp.TOTP("JBSWY3DPEHPK3PXP")
    context = {"data": "", "out": totp.now()}
    if request.method == "POST":
        context["data"] = str(request.POST["code"])
        context["out"]

    otp_text = pyotp.totp.TOTP("JBSWY3DPEHPK3PXP").provisioning_uri(
        name="idusp - 3135994", issuer_name="USP"
    )
    factory = qrcode.image.svg.SvgImage
    img = qrcode.make(otp_text, image_factory=factory, box_size=20)
    stream = BytesIO()
    img.save(stream)
    context["svg"] = stream.getvalue().decode()

    return render(request, "portal.html", context=context)


def index(request):
    return render(request, "index.html", {})


def new_pass(request):
    return render(request, "change_pass.html", {})
