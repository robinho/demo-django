from django.contrib import admin
from django.urls import path, include
from .views import index, portal, new_pass

urlpatterns = [
    path("", index),
    path("portal/", portal),
    path("forgot/", new_pass),
    path("first/", new_pass),
    path("auth/", include("django.contrib.auth.urls")),
    path("admin/", admin.site.urls),
]
